import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:controller/pages/trail.dart';
import 'package:controller/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sensors/sensors.dart';
//import 'package:sensors/sensors.dart';

class ControllerPage extends StatefulWidget {
  final String snailId;
  final String gameId;

  ControllerPage({this.snailId, this.gameId});
  @override
  _ControllerPageState createState() => _ControllerPageState();
}

class _ControllerPageState extends State<ControllerPage> {
  List snails = [];
  MyCustomNotifier notifier = MyCustomNotifier(value: false);

  List<double> _userAccelerometerValues;

  List<StreamSubscription<dynamic>> _streamSubscriptions =
      <StreamSubscription<dynamic>>[];

  double maxY = 0;
  bool isActive = false;
  @override
  Widget build(BuildContext context) {
    final List<String> userAccelerometer = _userAccelerometerValues
        ?.map((double v) => v.toStringAsFixed(2))
        ?.toList();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text('Race ${widget.gameId}',
            style: TextStyle(
                color: Color(0xFF62a921),
                fontWeight: FontWeight.bold,
                fontSize: 20)),
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor:
              isActive ? Color(0xFF62a921) : Color(0xFF62a921).withAlpha(13),
          child: Icon(Icons.home),
          onPressed: isActive
              ? () {
                  NotificationController().channel.sink.close();

                  snails.forEach((snail) {
                    snail["ready"] = false;
                    snail["occupied"] = false;
                  });

                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                        builder: (_) => TrailPage(
                              gameId: widget.gameId,
                              snails: snails,
                            )),
                  );
                }
              : null),
      body: StreamBuilder<Object>(
          stream: NotificationController().streamController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              print("Controler: " + snapshot.data);
              Map<String, dynamic> resp = jsonDecode(snapshot.data);
              if (resp["cmd"] != null && resp["cmd"] == "moveSnail") {
                if (resp["response"]["accepted"] == true) {
                  print("Snail moved");
                } else
                  return Text('${resp["response"]["error"]}');
              }

              if (resp["cmd"] != null && resp["cmd"] == "gameinfo") {
                snails = resp["response"]["snails"];
              }

              if (resp["cmd"] != null && resp["cmd"] == "gamestart") {
                print("game started");
                // start animation on controller
                notifier.start();
                snails = resp["response"]["snails"];
              }
              if (resp["cmd"] != null && resp["cmd"] == "gamefinish") {
                print("game finished");
                // display end result
                snails = resp["response"]["snails"];

                String winner;
                num maxSpeed = 0;

                snails.forEach((snail) {
                  if (snail["speed"] > maxSpeed) {
                    winner = snail["id"];
                    maxSpeed = snail["speed"];
                  }
                });

                if (!isActive) if (winner == widget.snailId)
                  Future.microtask(() {
                    setState(() {
                      isActive = true;
                    });

                    final snackBar = SnackBar(
                      duration: Duration(seconds: 6),
                      behavior: SnackBarBehavior.floating,
                      content: Text("Congratulation you won the race!"),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  });
                else
                  Future.microtask(() {
                    setState(() {
                      isActive = true;
                    });

                    final snackBar = SnackBar(
                      duration: Duration(seconds: 6),
                      behavior: SnackBarBehavior.floating,
                      content:
                          Text("Better luck next time, $winner won the race!"),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  });
              }
            }

            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
                    child: Text(
                        "Move your snail ${widget.snailId}, when the traffic light is green",
                        style: TextStyle(fontSize: 22)),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  TrafficLight(
                    notifier: notifier,
                    callback: () {
                      // Todo remove this BEGIN
                      if (maxY == 0)
                        maxY = (Random().nextDouble() * 30).roundToDouble();

                      // Todo remove this END

                      NotificationController().channel.sink.add(
                          '{"cmd": "moveSnail", "request": { "gameId" : "${widget.gameId}" , "snailId" : "${widget.snailId}", "speed" : $maxY }}');
                    },
                  ),
                ],
              ),
            );
          }),
    );
  }

  @override
  void dispose() {
    super.dispose();
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }

  @override
  void initState() {
    super.initState();

    NotificationController().channel.sink.add(
        '{ "cmd": "gameinfo", "request" : { "gameId": "${widget.gameId}" } }');

    if (kIsWeb) {
      // running on the web!
    } else {
      // NOT running on the web! You can check for additional platforms here.
      _streamSubscriptions
          .add(userAccelerometerEvents.listen((UserAccelerometerEvent event) {
        //print(event);
        _userAccelerometerValues = <double>[event.x, event.y, event.z];

        double y = event.y;

        if (maxY < y.abs()) {
          maxY = double.parse(y.abs().toStringAsFixed(1));
          print("Y: $maxY  - ${event.y}");
        }
      }));
    }
  }
}
