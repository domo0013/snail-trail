import 'dart:async';
import 'dart:convert';

import 'package:controller/pages/controller.dart';
import 'package:controller/utils.dart';
import 'package:flutter/material.dart';
//import 'package:sensors/sensors.dart';

class LobbyPage extends StatefulWidget {
  final String snailId;
  final String gameId;

  LobbyPage({this.snailId, this.gameId});
  @override
  _LobbyPageState createState() => _LobbyPageState();
}

class _LobbyPageState extends State<LobbyPage> {
  bool isready = false;
  bool lobbyisDone = false;
  List snails = [];
  Map<String, dynamic> mySnail;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text("In game ${widget.gameId}",
            style: TextStyle(
                color: Color(0xFF62a921),
                fontWeight: FontWeight.bold,
                fontSize: 20)),
        actions: [
          StreamBuilder<Object>(
              stream: NotificationController().streamController.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  print("Controler: " + snapshot.data);
                  Map<String, dynamic> resp = jsonDecode(snapshot.data);
                  if (resp["cmd"] != null && resp["cmd"] == "moveSnail") {
                    if (resp["response"]["accepted"] == true) {
                      print("Snail moved");
                    } else
                      return Text('${resp["response"]["error"]}');
                  }

                  if (resp["cmd"] != null && resp["cmd"] == "ready") {
                    if (resp["response"]["accepted"] == true) {
                      print("Snail is ready");
                      snails.forEach((element) {
                        if (element["id"] == widget.snailId)
                          element["ready"] = true;
                      });
                      isready = true;

                      int occCnt = 0;
                      int rdyCnt = 0;

                      snails.forEach((element) {
                        if (element["occupied"]) occCnt++;
                        if (element["ready"]) {
                          rdyCnt++;
                        }
                      });

                      if (!lobbyisDone && occCnt > 0 && occCnt == rdyCnt) {
                        lobbyisDone = true;
                        Future.microtask(() => Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ControllerPage(
                                      snailId: widget.snailId,
                                      gameId: widget.gameId,
                                    ))));
                      }
                    } else
                      Text('${resp["response"]["error"]}');
                  }

                  if (resp["cmd"] != null && resp["cmd"] == "gameinfo") {
                    snails = resp["response"]["snails"];
                    snails.forEach((element) {
                      if (element["id"] == widget.snailId) mySnail = element;
                    });

                    int occCnt = 0;
                    int rdyCnt = 0;

                    snails.forEach((element) {
                      if (element["occupied"]) occCnt++;
                      if (element["ready"]) {
                        rdyCnt++;
                      }
                    });

                    if (!lobbyisDone && occCnt > 0 && occCnt == rdyCnt) {
                      lobbyisDone = true;
                      Future.microtask(() => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ControllerPage(
                                    snailId: widget.snailId,
                                    gameId: widget.gameId,
                                  ))));
                    }
                  }
                  Future.microtask(() => setState(() {}));

                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: InkWell(
                      onTap: () {
                        final snackBar = SnackBar(
                            behavior: SnackBarBehavior.floating,
                            content: Text(NotificationController().disconnected
                                ? "No connection"
                                : "Connected"));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      },
                      child: Icon(
                        NotificationController().disconnected
                            ? Icons.error
                            : Icons.check,
                        color: NotificationController().disconnected
                            ? Colors.red
                            : Colors.white,
                      ),
                    ),
                  );
                }
                return Padding(
                  padding: const EdgeInsets.all(10),
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                );
              })
        ],
      ),
      body: Center(
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Competitor snails:", style: TextStyle(fontSize: 20)),
                Wrap(
                  children: [
                    ...snails.map((e) {
                      if (e["id"] != widget.snailId)
                        return SnailCard(snail: e);
                      else
                        return SizedBox(
                          width: 0,
                        );
                    }).toList()
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                  child: Text(
                    "Waiting for players to get ready...",
                    style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SnailCard(
                  snail: mySnail,
                  width: 300 / 2.25,
                  height: 300,
                ),
                ElevatedButton(
                    onPressed: () {
                      NotificationController().channel.sink.add(
                          '{ "cmd": "ready", "request" : { "gameId": "${widget.gameId}", "snailId" : "${widget.snailId}" } }');

                      isready = true;
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Ready!"),
                    )),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "(scroll down for traffic explanation)",
                  style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                      child: Text(
                        "Traffic light hints:",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TrafficLightStatic(
                            state: TrafficLightState.red,
                          ),
                        ),
                        Text(
                          "When the Traffic light is red, the Snail hasn't been occupied yet.",
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TrafficLightStatic(
                            state: TrafficLightState.yellow,
                          ),
                        ),
                        Text(
                          "When the Traffic light is yellow, the snail is occupied.",
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TrafficLightStatic(
                            state: TrafficLightState.green,
                          ),
                        ),
                        Text(
                          "When the Traffic light is green, the Snail is occupied and ready.",
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    NotificationController().channel.sink.add(
        '{ "cmd": "gameinfo", "request" : { "gameId": "${widget.gameId}" } }');
  }
}
