import 'dart:convert';

import 'package:controller/pages/lobby.dart';
import 'package:flutter/material.dart';
import 'package:controller/utils.dart';

//String userInfo;= nickName;

class TrailPage extends StatefulWidget {
  final List snails;
  final String gameId;
  TrailPage({
    this.snails,
    Key key,
    this.gameId,
  }) : super(key: key);

  @override
  _TrailPageState createState() => _TrailPageState();
}

class _TrailPageState extends State<TrailPage> {
  final snailController = TextEditingController();
  bool validate = false;
  List snails;

  @override
  void initState() {
    super.initState();
    snails = widget.snails;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.black87,
          title: Text(
            "Welcome to game ${widget.gameId}",
            style: TextStyle(
                color: Color(0xFF62a921),
                fontWeight: FontWeight.bold,
                fontSize: 20),
          ),
          actions: [
            StreamBuilder(
              stream: NotificationController().streamController.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  print("Trail: " + snapshot.data);
                  // snapshot.data is the repsonse from WS server
                  Map<String, dynamic> resp =
                      jsonDecode(snapshot.data as String);

                  if (resp["cmd"] != null && resp["cmd"] == "gameslist") {
                    resp["response"]["games"].forEach((game) {
                      print(game["id"] + 'checking');
                      if (game["id"] == widget.gameId) {
                        print(game["id"] + 'found');
                        Future.microtask(() => setState(() {
                              snails = game["snails"];
                            }));
                      }
                    });
                  }
                  if (resp["cmd"] != null && resp["cmd"] == "join") {
                    if (resp["response"]["accepted"] == true) {
                      print("Join accepted");
                      Future.microtask(() => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LobbyPage(
                                    snailId: resp["response"]["snailId"],
                                    gameId: widget.gameId,
                                  ))));
                    } else
                      Future.microtask(() {
                        final snackBar = SnackBar(
                          behavior: SnackBarBehavior.floating,
                          content: Text(resp["response"]["error"]),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      });
                  }
                }
                return Padding(
                    padding: const EdgeInsets.all(10),
                    child: InkWell(
                      onTap: () {
                        final snackBar = SnackBar(
                            behavior: SnackBarBehavior.floating,
                            content: Text(NotificationController().disconnected
                                ? "No connection"
                                : "Connected"));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      },
                      child: Icon(
                        NotificationController().disconnected
                            ? Icons.error
                            : Icons.check,
                        color: NotificationController().disconnected
                            ? Colors.red
                            : Colors.white,
                      ),
                    ));
              },
            ),
          ]),
      // bottomNavigationBar: BottomAppBar(
      //     child: Container(
      //         height: 60,
      //         color: Colors.black87,
      //         child:
      //             Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      //           Text(
      //             "Welcome to game ${widget.gameId}",
      //             style: TextStyle(
      //                 color: Color(0xFF62a921),
      //                 fontWeight: FontWeight.bold,
      //                 fontSize: 20),
      //           )
      //         ]))),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            'Choose a Snail',
            style: TextStyle(fontSize: 40),
          ),
          SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
            child: Text(
              "Choose the Snail that you find suitable for your glorious race",
              style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            ),
          ),
          SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Wrap(
              alignment: WrapAlignment.center,
              children: [
                ...snails.map((s) {
                  return Container(
                    //width: 20,
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: InkWell(
                        onTap: () {
                          NotificationController().channel.sink.add(
                              '{ "cmd": "join", "request" : { "gameId": "${widget.gameId}", "snailId" : "${s["id"]}" }}');
                        },
                        child: SnailCard(
                          snail: s,
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "(Pro-Tip: It really doesn't matter which one you choose)",
            style: TextStyle(fontSize: 15, fontStyle: FontStyle.italic),
          ),
        ]),
      ),
    );
  }
}
