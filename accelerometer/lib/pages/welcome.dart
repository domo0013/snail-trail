import 'dart:convert';

import 'package:controller/utils.dart';
import 'package:flutter/material.dart';
import 'package:controller/pages/trail.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({
    Key key,
  }) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title = "Welcome to Snail Trail";

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final trailController = TextEditingController();
  bool validate = false;
  String game = "...";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text(
          "Welcome to SnailTrail",
          style: Theme.of(context).textTheme.headline5,
        ),
        actions: [
          StreamBuilder<Object>(
              stream: NotificationController().streamController.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  print("Welcome: " + snapshot.data);
                  Map<String, dynamic> resp = jsonDecode(snapshot.data);
                  if (resp["cmd"] != null && resp["cmd"] == "gameinfo") {
                    if (resp["response"]["error"] == null)
                      Future.microtask(() => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TrailPage(
                                    snails: resp["response"]["snails"],
                                    gameId: resp["response"]["id"],
                                  ))));
                    else {
                      Future.microtask(() {
                        final snackBar = SnackBar(
                          behavior: SnackBarBehavior.floating,
                          content: Text(resp["response"]["error"]),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      });
                    }
                  }

                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: InkWell(
                      onTap: () {
                        final snackBar = SnackBar(
                            behavior: SnackBarBehavior.floating,
                            content: Text(NotificationController().disconnected
                                ? "No connection"
                                : "Connected"));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      },
                      child: Icon(
                        NotificationController().disconnected
                            ? Icons.error
                            : Icons.check,
                        color: NotificationController().disconnected
                            ? Colors.red
                            : Colors.white,
                      ),
                    ),
                  );
                }
                return Padding(
                  padding: const EdgeInsets.all(10),
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                );
              }),
        ],
      ),
      // bottomNavigationBar: BottomAppBar(
      //     child: Container(
      //         height: 60,
      //         color: Colors.black87,
      //         child:
      //             Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      //           Text(
      //             "Welcome to SnailTrail",
      //             style: TextStyle(
      //                 color: Color(0xFF62a921),
      //                 fontWeight: FontWeight.bold,
      //                 fontSize: 20),
      //           )
      //         ]))),
      body: ListView(
        shrinkWrap: true,
        children: [
          Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              SizedBox(
                height: 20,
              ),
              Container(
                  height: 80,
                  child: Image.asset("assets/images/snailLogo.png")),
              Text(
                "Join trail",
                style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 100),
              Padding(
                padding: const EdgeInsets.fromLTRB(75, 0, 75, 0),
                child: Center(
                  child: Text(
                      "Enter the 5 digit trail ID to join the race among other snails and race for victory!",
                      style: TextStyle(
                        fontSize: 20, //fontStyle: FontStyle.italic),
                      )),
                ),
              ),
              SizedBox(height: 40),
              //TrafficLight(notifier: MyCustomNotifier(value: true)),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 300,
                  height: 50,
                  child: TextField(
                    obscureText: false,
                    controller: trailController,
                    decoration: InputDecoration(
                      errorText: validate ? "Enter trail ID" : null,
                      border: OutlineInputBorder(),
                      labelText: 'SnailTrail ID',
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () {
                    NotificationController().channel.sink.add(
                        '{ "cmd": "gameinfo", "request" : { "gameId": "${trailController.text}" } }');
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      "Join Race",
                      style: Theme.of(context).textTheme.button,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Center(
                  child: Text(
                    "Go to 'www.snailtrail.fun' to create games or join an existing one",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              )
            ]),
          ),
        ],
      ),
    );
  }
}
