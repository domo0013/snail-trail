import 'dart:async';
import 'dart:math';

import 'package:controller/snail.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;

class NotificationController {
  static final NotificationController _singleton =
      new NotificationController._internal();

  StreamController<String> streamController =
      new StreamController.broadcast(sync: true);

  String wsUrl = 'wss://snailtrail.glitch.me/ws';

  bool disconnected = true;

  WebSocketChannel channel;

  factory NotificationController() {
    return _singleton;
  }

  NotificationController._internal() {
    initWebSocketConnection();
  }

  void setConnected(bool isConnected) {
    disconnected = !isConnected;
    if (isConnected)
      streamController.add('{ "status": "Connected" }');
    else
      streamController.add('{ "status": "Disconnected" }');
  }

  initWebSocketConnection() async {
    await Future.delayed(Duration(seconds: 3));
    print("conecting...");
    this.channel?.sink?.close();

    this.channel = await connectWs();
    await Future.delayed(Duration(seconds: 3));
    print("socket connection initializied");
    broadcastNotifications();
  }

  broadcastNotifications() {
    try {
      this.channel.stream.listen((streamData) {
        setConnected(true);
        print(streamData);
        streamController.add(streamData);
      }, onDone: () {
        print("conecting aborted");
        setConnected(false);

        initWebSocketConnection();
      }, onError: (e) {
        print('Server error: $e');
        setConnected(false);

        initWebSocketConnection();
      });
    } catch (e) {
      print("Failed to listen to stream!");
    }
  }

  connectWs() async {
    try {
      return WebSocketChannel.connect(
        Uri.parse(wsUrl),
      );
    } catch (e) {
      print("Error! can not connect WS connectWs " + e.toString());
      setConnected(false);
      channel.sink.close(status.goingAway);
      return await connectWs();
    }
  }
}

class TrafficLightStatic extends StatelessWidget {
  final TrafficLightState state;

  final double size;

  const TrafficLightStatic({
    this.state = TrafficLightState.red,
    this.size,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      child: Column(
        children: [
          Column(
            children: [
              Icon(
                Icons.circle,
                color: state == TrafficLightState.red
                    ? Colors.red
                    : Colors.red.withAlpha(65),
                size: size,
              ),
              Icon(
                Icons.circle,
                color: state == TrafficLightState.yellow
                    ? Colors.yellow
                    : Colors.yellow.withAlpha(65),
                size: size,
              ),
              Icon(
                Icons.circle,
                color: state == TrafficLightState.green
                    ? Colors.green
                    : Colors.green.withAlpha(65),
                size: size,
              )
            ],
          ),
        ],
      ),
      decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.all(Radius.circular(20))),
    );
  }
}

enum TrafficLightState { red, yellow, green, idle }

class MyCustomNotifier extends ValueNotifier<bool> {
  MyCustomNotifier({bool value}) : super(value ?? false);

  void start() {
    // Add your super logic here!
    value = true;
  }

  void stop() {
    // Add your super logic here!
    value = false;
  }
}

class TrafficLight extends StatefulWidget {
  final MyCustomNotifier notifier;
  final TrafficLightState state;
  final Function callback;
  const TrafficLight({
    this.notifier,
    this.callback,
    this.state = TrafficLightState.idle,
    Key key,
  }) : super(key: key);

  @override
  _TrafficLightState createState() => _TrafficLightState();
}

class _TrafficLightState extends State<TrafficLight>
    with TickerProviderStateMixin {
  Animation<double> animationSize;
  Animation<Color> animationRed;
  Animation<Color> animationYellow;
  Animation<Color> animationGreen;
  Animation<double> animationCounter;
  TrafficLightState trafficLightState;
  AnimationController controller;

  MyCustomNotifier _notifier;

  @override
  void initState() {
    _notifier = widget.notifier;
    controller =
        AnimationController(duration: const Duration(seconds: 6), vsync: this);

    animationCounter = Tween<double>(begin: 3, end: 0).animate(CurvedAnimation(
        parent: controller, curve: Interval(0.1, 0.8, curve: Curves.linear)))
      ..addListener(() {
        setState(() {});
      });
    animationSize = Tween<double>(begin: 0, end: 120).animate(CurvedAnimation(
        parent: controller, curve: Interval(0, 0.1, curve: Curves.easeIn)))
      ..addListener(() {
        setState(() {});
      });
    animationRed = ColorTween(begin: Colors.red, end: Colors.red.withAlpha(65))
        .animate(CurvedAnimation(
            parent: controller,
            curve: Interval(0.1, 0.4, curve: Curves.easeIn)))
          ..addListener(() {
            setState(() {});
          });

    animationYellow = TweenSequence([
      TweenSequenceItem(
          tween: ColorTween(
              begin: Colors.yellow.withAlpha(65), end: Colors.yellow),
          weight: 1),
      TweenSequenceItem(
          tween: ColorTween(
              begin: Colors.yellow, end: Colors.yellow.withAlpha(65)),
          weight: 1008),
    ]).animate(CurvedAnimation(
        parent: controller, curve: Interval(0.4, 0.8, curve: Curves.easeOut)))
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          animationYellow = ColorTween(
                  begin: Colors.yellow.withAlpha(65),
                  end: Colors.yellow.withAlpha(65))
              .animate(controller);
        }
      })
      ..addListener(() {
        setState(() {});
      });

    animationGreen = TweenSequence([
      TweenSequenceItem(
          tween:
              ColorTween(begin: Colors.green.withAlpha(65), end: Colors.green),
          weight: 1),
      TweenSequenceItem(
          tween: ColorTween(begin: Colors.green, end: Colors.greenAccent),
          weight: 108),
    ]).animate(CurvedAnimation(
        parent: controller, curve: Interval(0.7, 0.9, curve: Curves.easeIn)))
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          widget.callback();
          setState(() {
            trafficLightState = TrafficLightState.red;
          });

          // CALL widget finish callback here!!!!
        }
      });
    trafficLightState = widget.state;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
        valueListenable: _notifier,
        builder: (context, isStarted, child) {
          if (isStarted == true) controller.forward();

          if (trafficLightState == TrafficLightState.idle)
            return Container(
              width: animationSize.value,
              child: Column(
                children: [
                  Text(animationCounter.value.toStringAsFixed(0),
                      style: TextStyle(color: Colors.white)),
                  Column(
                    children: [
                      Icon(
                        Icons.circle,
                        color: animationRed.value,
                        size: animationSize.value,
                      ),
                      Icon(
                        Icons.circle,
                        color: animationYellow.value,
                        size: animationSize.value,
                      ),
                      Icon(
                        Icons.circle,
                        color: animationGreen.value,
                        size: animationSize.value,
                      )
                    ],
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
            );
          else {
            return Container(
              width: animationSize.value,
              child: Column(
                children: [
                  Text(animationCounter.value.toStringAsFixed(0),
                      style: TextStyle(color: Colors.white)),
                  Column(
                    children: [
                      Icon(
                        Icons.circle,
                        color: trafficLightState == TrafficLightState.red
                            ? Colors.red
                            : Colors.red.withAlpha(65),
                        size: animationSize.value,
                      ),
                      Icon(
                        Icons.circle,
                        color: trafficLightState == TrafficLightState.yellow
                            ? Colors.yellow
                            : Colors.yellow.withAlpha(65),
                        size: animationSize.value,
                      ),
                      Icon(
                        Icons.circle,
                        color: trafficLightState == TrafficLightState.green
                            ? Colors.green
                            : Colors.green.withAlpha(65),
                        size: animationSize.value,
                      )
                    ],
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
            );
          }
        });
  }
}

class SnailCard extends StatelessWidget {
  final Map<String, dynamic> snail;
  final double height;
  final double width;
  const SnailCard({
    Key key,
    this.height = 180,
    this.width = 180 / 2.25,
    this.snail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (snail == null) return CircularProgressIndicator();
    return Card(
      color: Colors.accents[snail["color"] * 2].withAlpha(45),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(snail["id"],
                style: TextStyle(
                  fontSize: width / 4,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                )),
          ),
          Container(
              width: width,
              child: Row(
                children: [
                  SnailWidget(
                    color: Colors.accents[snail["color"] * 2],
                    height: height / 1.5,
                    width: (width / 2),
                  ),
                  Container(
                    height: height / 3,
                    width: (width / 4),
                    child: TrafficLightStatic(
                      size: width / 4,
                      state: snail["occupied"] && snail["ready"]
                          ? TrafficLightState.green
                          : snail["occupied"] && !snail["ready"]
                              ? TrafficLightState.yellow
                              : TrafficLightState.red,
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}

MaterialColor generateMaterialColor(Color color) {
  return MaterialColor(color.value, {
    50: tintColor(color, 0.9),
    100: tintColor(color, 0.8),
    200: tintColor(color, 0.6),
    300: tintColor(color, 0.4),
    400: tintColor(color, 0.2),
    500: color,
    600: shadeColor(color, 0.1),
    700: shadeColor(color, 0.2),
    800: shadeColor(color, 0.3),
    900: shadeColor(color, 0.4),
  });
}

int tintValue(int value, double factor) =>
    max(0, min((value + ((255 - value) * factor)).round(), 255));

Color tintColor(Color color, double factor) => Color.fromRGBO(
    tintValue(color.red, factor),
    tintValue(color.green, factor),
    tintValue(color.blue, factor),
    1);

int shadeValue(int value, double factor) =>
    max(0, min(value - (value * factor).round(), 255));

Color shadeColor(Color color, double factor) => Color.fromRGBO(
    shadeValue(color.red, factor),
    shadeValue(color.green, factor),
    shadeValue(color.blue, factor),
    1);
