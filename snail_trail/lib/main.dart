// @dart=2.9

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:snail_trail/pages/gamelist.dart';

import 'utils.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        buttonColor: Colors.black38,
        textTheme: TextTheme(
          headline1: GoogleFonts.bebasNeue(
              fontSize: 97, fontWeight: FontWeight.w300, letterSpacing: -1.5),
          headline2: GoogleFonts.bebasNeue(
              fontSize: 61, fontWeight: FontWeight.w300, letterSpacing: -0.5),
          headline3:
              GoogleFonts.bebasNeue(fontSize: 48, fontWeight: FontWeight.w400),
          headline4: GoogleFonts.bebasNeue(
              fontSize: 34, fontWeight: FontWeight.w400, letterSpacing: 0.25),
          headline5: GoogleFonts.bebasNeue(
              textStyle: TextStyle(color: Color(0xFF62a921)),
              fontSize: 24,
              fontWeight: FontWeight.w400),
          headline6: GoogleFonts.bebasNeue(
              fontSize: 20, fontWeight: FontWeight.w500, letterSpacing: 0.15),
          subtitle1: GoogleFonts.bebasNeue(
              fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.15),
          subtitle2: GoogleFonts.bebasNeue(
              fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1),
          bodyText1: GoogleFonts.bebasNeue(
              fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.5),
          bodyText2: GoogleFonts.bebasNeue(
              fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
          button: GoogleFonts.bebasNeue(
              textStyle: TextStyle(color: Color(0xFF62a921)),
              fontSize: 18,
              fontWeight: FontWeight.w500,
              letterSpacing: 1.25),
          caption: GoogleFonts.bebasNeue(
              fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
          overline: GoogleFonts.bebasNeue(
              fontSize: 35, fontWeight: FontWeight.w400, letterSpacing: 1.5),
        ),
        primarySwatch: generateMaterialColor(Color(0xFF62a921)),
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ButtonStyle(
                elevation: MaterialStateProperty.all<double>(4.0),
                foregroundColor:
                    MaterialStateProperty.all<Color>(Color(0xFF62a921)),
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.black87))),
      ),
      home: SnailGameListPage(),
    );
  }
}
