import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:snail_trail/pages/gamedetails.dart';

import '../utils.dart';

class SnailGameListPage extends StatefulWidget {
  @override
  _SnailGameListPageState createState() => _SnailGameListPageState();
}

class _SnailGameListPageState extends State<SnailGameListPage> {
  var _streamController;
  List<Widget> gameWidgets = [];
  List gameStates = [];
  final trailController = TextEditingController();
  bool validate = false;

  @override
  void initState() {
    super.initState();

    _streamController = NotificationController().streamController;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void refreshSearchRequest(newFilter) {
    print(newFilter);

    setState(() {
      updateGameWidgets(newFilter);
    });
  }

  static const Duration _noInputDelay = Duration(milliseconds: 350);
  String _freeTextSearchFilter = '';
  void onFreeTextSearchChange(String text) {
    String newFilter = text.trim();
    if (_freeTextSearchFilter.toLowerCase() != newFilter.toLowerCase()) {
      newFilter = newFilter.isNotEmpty ? newFilter : '';
      _freeTextSearchFilter = newFilter;

      Future<void>.delayed(_noInputDelay).then((_) {
        if (_freeTextSearchFilter == newFilter) {
          refreshSearchRequest(newFilter);
        }
      });
    }
  }

  void updateGameWidgets(String filter) {
    gameWidgets = [];
    gameStates.forEach((e) {
      int occupiedSnail = 0;
      e["snails"].forEach((s) {
        if (s["occupied"]) occupiedSnail++;
      });

      var gameWidget = ExpansionTile(
        title: InkWell(
          onTap: () {
            NotificationController().channel.sink.add(
                '{ "cmd": "spectate", "request" : { "gameId": "${e["id"]}" } }');
          },
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  child: Icon(Icons.copy),
                  onTap: () {
                    Clipboard.setData(ClipboardData(text: e["id"]));
                    final snackBar = SnackBar(
                      behavior: SnackBarBehavior.floating,
                      content: Text("Game ID ${e["id"]} copied to clipboard"),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  },
                ),
              ),
              Text(
                ' ${e["id"]}    $occupiedSnail/${e["snails"].length}',
                style: Theme.of(context).textTheme.overline,
              ),
            ],
          ),
        ),
        children: <Widget>[
          ListTile(
            title: Column(
              children: [
                ...e["snails"].map((s) {
                  return Container(
                    color: s["occupied"] ? Colors.black12 : Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          Icons.circle,
                          color: Colors.accents[s["color"] * 2],
                          size: 20,
                        ),
                        Text(
                          s["id"],
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700,
                              color: Colors.accents[s["color"] * 2]),
                        ),
                        Text(s["occupied"] ? "inGame" : "free"),
                        Text(s["ready"] ? "ready" : "waiting"),
                        Text(s["speed"].toString())
                      ],
                    ),
                  );
                }).toList(),
              ],
            ),
          )
        ],
      );
      if (filter.isEmpty || e["id"].contains(filter.toUpperCase()))
        gameWidgets.add(gameWidget);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text("Welcome to www.snailtrail.fun",
            style: TextStyle(
                color: Color(0xFF62a921),
                fontWeight: FontWeight.bold,
                fontSize: 20)),
        actions: [
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.refresh,
                color: Color(0xFF62a921),
              ),
            ),
            onTap: () {
              NotificationController()
                  .channel
                  .sink
                  .add('{ "cmd": "gameslist", "payload" : { }}');
            },
          )
        ],
      ),
      body: StreamBuilder<Object>(
          stream: _streamController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              // got messge snapshot.data from server
              print("Games: " + snapshot.data);
              Map<String, dynamic> resp = jsonDecode(snapshot.data);

              if (resp["cmd"] == "welcome")
                NotificationController()
                    .channel
                    .sink
                    .add('{ "cmd": "gameslist", "payload" : { }}');
              else if (resp["cmd"] == "create")
                NotificationController()
                    .channel
                    .sink
                    .add('{ "cmd": "gameslist", "payload" : { }}');
              else if (resp["cmd"] == "gameslist") {
                gameStates = resp["response"]["games"];
              } else if (resp["cmd"] == "gameinfo") {
                // TODO move to GAME Page
                print("Got gameinfo spectator joined");
                Future.microtask(() => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GameDetailsPage(
                              gameInfo: resp["response"],
                            ))));
              }

              updateGameWidgets(trailController.text);

              // print(" $gameId has ${snails}");

              return Container(
                width: double.infinity,
                color: NotificationController().disconnected
                    ? Colors.red.withAlpha(128)
                    : Color(0xFF62a921).withAlpha(128),
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        "Enter Game ID:",
                        style: TextStyle(fontSize: 30),
                      ),
                    ),
                    //TrafficLight(notifier: MyCustomNotifier(value: true)),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: 300,
                        height: 50,
                        child: TextField(
                          controller: trailController,
                          onChanged: onFreeTextSearchChange,
                          decoration: InputDecoration(
                            hintText: "Search",
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.only(
                                left: 13.5, bottom: 11, top: 13.5),
                            suffixIcon: Icon(Icons.search),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          //Spacer(),
                          ElevatedButton(
                            onPressed: () {
                              NotificationController().channel.sink.add(
                                  '{ "cmd": "spectate", "request" : { "gameId": "${trailController.text}" } }');
                            },
                            child: Text(
                              "Spectate Trail",
                              style: TextStyle(fontSize: 30),
                            ),
                          ),
                          //Spacer(),
                        ],
                      ),
                    ),
                    ...gameWidgets
                  ],
                ),
              );
            } else
              return Container();
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // send messge snapshot.data from server

          NotificationController()
              .channel
              .sink
              .add('{ "cmd": "create", "payload" : { "players" : 5}}');
        },
        tooltip: 'Create',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
