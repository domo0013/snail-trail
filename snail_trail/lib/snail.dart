import 'package:flutter/material.dart';
import 'dart:math';

extension on AnimationController {
  void repeatEx({@required int times}) {
    var count = 0;
    addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (++count < times) {
          reverse();
        }
      } else if (status == AnimationStatus.dismissed) {
        forward();
      }
    });
  }
}

class SnailAnimation {
  Animation<double> eyesLength;
  Animation<double> eyesAngle;
  Animation<double> houseOffset;
  Animation<double> forward;
  Animation<double> height;

  SnailAnimation(
      {this.eyesAngle,
      this.eyesLength,
      this.forward,
      this.height,
      this.houseOffset});
}

class AnimationDirector {
  Map<String, SnailAnimation> animations = {
    "idle": SnailAnimation(),
    "happy": SnailAnimation(),
    "run": SnailAnimation()
  };
  AnimationController _controller;

  void init(AnimationController controller) {
    _controller = controller;

    animations["idle"].eyesAngle = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 16.0, end: 45.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 45.0, end: 30.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 30.0, end: 50.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 50.0, end: 16.0), weight: 1)
    ]).animate(
      _controller,
    );
    animations["idle"].eyesLength = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 1.0, end: 1.2), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 1.2, end: 0.8), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.8, end: 0.6), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.6, end: 1.5), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 1.5, end: 0.8), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.8, end: 1.0), weight: 1)
    ]).animate(
      _controller,
    );
    animations["idle"].houseOffset = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: -0.1, end: 0.1), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.1, end: -0.1), weight: 1)
    ]).animate(
      _controller,
    );
    animations["idle"].forward = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: -0.2, end: 0.2), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.2, end: -0.2), weight: 1)
    ]).animate(
      _controller,
    );
    animations["idle"].height = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 180.0, end: 200.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 200.0, end: 180.0), weight: 1)
    ]).animate(
      _controller,
    );

    animations["happy"].eyesAngle = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 16.0, end: 145.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 145.0, end: 16.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 16.0, end: 145.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 145.0, end: 16.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 16.0, end: 145.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 145.0, end: 16.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 16.0, end: 145.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 145.0, end: 16.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 16.0, end: 145.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 145.0, end: 16.0), weight: 1),
    ]).animate(
      _controller,
    );
    animations["happy"].eyesLength = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 1.0, end: 0.2), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.2, end: 0.8), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.8, end: 0.6), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.6, end: 1.5), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 1.5, end: 0.8), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.8, end: 1.0), weight: 1)
    ]).animate(
      _controller,
    );
    animations["happy"].houseOffset = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: -0.1, end: 0.1), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.1, end: -0.1), weight: 1)
    ]).animate(
      _controller,
    );
    animations["happy"].forward = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: -0.2, end: 0.6), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.6, end: -0.2), weight: 1)
    ]).animate(
      _controller,
    );
    animations["happy"].height = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 280.0, end: 380.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 380.0, end: 280.0), weight: 1)
    ]).animate(
      _controller,
    );

    animations["run"].eyesAngle = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 16.0, end: 155.0), weight: 2),
      TweenSequenceItem(tween: Tween(begin: 155.0, end: 155.0), weight: 4),
      TweenSequenceItem(tween: Tween(begin: 155.0, end: 16.0), weight: 1),
    ]).animate(
      _controller,
    );
    animations["run"].eyesLength = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 1.0, end: 1.4), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 1.4, end: 1.0), weight: 1),
    ]).animate(
      _controller,
    );
    animations["run"].houseOffset = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: -0.2, end: 0.2), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.2, end: -0.2), weight: 1)
    ]).animate(
      _controller,
    );
    animations["run"].forward = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: -0.2, end: 0.6), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 0.6, end: -0.2), weight: 1)
    ]).animate(
      _controller,
    );
    animations["run"].height = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 280.0, end: 320.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 320.0, end: 280.0), weight: 1)
    ]).animate(
      _controller,
    );
  }
}

class SnailWidget extends StatefulWidget {
  final double forward;
  final double houseOffsetRatio;
  final Color color;
  final double eyeLengthRatio;
  final double eyeAngle;
  final double width;
  final double height;
  final Offset offset;

  SnailWidget(
      {this.forward = 0.2,
      this.houseOffsetRatio = 0.1,
      this.color,
      this.eyeLengthRatio = 1.5,
      this.width,
      this.height,
      this.offset,
      this.eyeAngle = 35});
  // You can ask Get to find a Controller that is being used by another page and redirect you to it.
  @override
  _SnailWidgetState createState() => _SnailWidgetState();
}

class _SnailWidgetState extends State<SnailWidget>
    with TickerProviderStateMixin {
  Animation<double> eyesLength;
  Animation<double> eyesAngle;
  Animation<double> houseOffset;
  Animation<double> forward;
  Animation<double> height;

  AnimationController _controller;
  AnimationDirector director = AnimationDirector();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 3));

    _controller.addListener(() {
      setState(() {});
    });
    director.init(_controller);

    startAnimation("idle", Duration(seconds: 3), 50000);
  }

  void startAnimation(String anim, Duration duration, int times) {
    _controller.reset();
    _controller.duration =
        duration + Duration(milliseconds: Random().nextInt(800));

    eyesLength = director.animations[anim].eyesLength;
    eyesAngle = director.animations[anim].eyesAngle;
    houseOffset = director.animations[anim].houseOffset;
    forward = director.animations[anim].forward;
    height = director.animations[anim].height;

    _controller.repeatEx(times: times);
    _controller.forward();
  }

  @override
  Widget build(context) {
    // Access the updated count variable
    return Container(
        width: widget.width,
        height: height.value,
        child: CustomPaint(
            painter: SnailPainter(
          forward: forward.value,
          houseOffsetRatio: houseOffset.value,
          color: widget.color,
          eyeAngle: eyesAngle.value,
          eyeLengthRatio: eyesLength.value,
        )));
  }
}

class SnailPainter extends CustomPainter {
  final double forward;
  final double houseOffsetRatio;
  final Color color;
  final double eyeLengthRatio;
  final double eyeAngle;
  SnailPainter(
      {this.forward,
      this.houseOffsetRatio,
      this.color,
      this.eyeLengthRatio = 1,
      this.eyeAngle = 45});

  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint

    num centerX = size.width / 2;
    num centerY = size.height / 2;
    num radius = min(centerX, centerY);
    Offset center = Offset(centerX, centerY);
    Paint fillBrush = Paint()..color = color;
    Paint outBrush = Paint()
      ..color = Colors.black45
      ..strokeWidth = 30 * (radius / 300)
      ..style = PaintingStyle.stroke;

    Paint headBrush = Paint()
      ..color = color
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    Paint headBruchOutline = Paint()
      ..color = Colors.black45
      ..style = PaintingStyle.stroke
      ..strokeWidth = 3;

    Paint eyeBrush = Paint()
      ..color = color
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 16 * (radius / 150)
      ..style = PaintingStyle.stroke;

// body fill
    canvas.drawRRect(
        RRect.fromRectAndRadius(
            Rect.fromCenter(
                center: center + Offset(0, radius * -forward),
                width: size.width / 4,
                height: size.height * 0.6),
            Radius.circular(16)),
        headBrush);
//body outline
    canvas.drawRRect(
        RRect.fromRectAndRadius(
            Rect.fromCenter(
                center: center + Offset(0, radius * -forward),
                width: size.width / 4,
                height: size.height * 0.6),
            Radius.circular(16)),
        headBruchOutline);

    // snailhouse

    canvas.drawCircle(center, radius * 0.7, fillBrush);

    // left eye handle

    double eyeLength = radius * 0.6 * eyeLengthRatio;

    Offset head =
        center + Offset(0, radius * -forward - size.height * 0.3 + 16);

    // left eye
    canvas.drawCircle(
        head +
            Offset(eyeLength * cos((-90 + -eyeAngle) * pi / 180),
                eyeLength * sin((-90 - eyeAngle) * pi / 180)),
        radius * 0.1,
        fillBrush);
    canvas.drawCircle(
        head +
            Offset(eyeLength * cos((-90 + -eyeAngle) * pi / 180),
                eyeLength * sin((-90 - eyeAngle) * pi / 180)),
        radius * 0.1,
        headBruchOutline);

    // left eye handle

    canvas.drawLine(
        head,
        head +
            Offset(eyeLength * cos((-90 + -eyeAngle) * pi / 180),
                eyeLength * sin((-90 - eyeAngle) * pi / 180)),
        eyeBrush);

    // left pupil
    canvas.drawCircle(
        head +
            Offset(eyeLength * cos((-90 + -eyeAngle) * pi / 180),
                eyeLength * sin((-90 - eyeAngle) * pi / 180)),
        radius * 0.005,
        outBrush);

    // right eye
    canvas.drawCircle(
        head +
            Offset(eyeLength * cos((-90 + eyeAngle) * pi / 180),
                eyeLength * sin((-90 + eyeAngle) * pi / 180)),
        radius * 0.1,
        fillBrush);
    canvas.drawCircle(
        head +
            Offset(eyeLength * cos((-90 + eyeAngle) * pi / 180),
                eyeLength * sin((-90 + eyeAngle) * pi / 180)),
        radius * 0.1,
        headBruchOutline);

    // right eye handle

    canvas.drawLine(
        head,
        head +
            Offset(eyeLength * cos((-90 + eyeAngle) * pi / 180),
                eyeLength * sin((-90 + eyeAngle) * pi / 180)),
        eyeBrush);

//right pupil

    canvas.drawCircle(
        head +
            Offset(eyeLength * cos((-90 + eyeAngle) * pi / 180),
                eyeLength * sin((-90 + eyeAngle) * pi / 180)),
        radius * 0.005,
        outBrush);

// house contour
    canvas.drawCircle(center, radius * 0.68, outBrush);
    canvas.drawCircle(center + Offset(0, radius * -houseOffsetRatio),
        radius * 0.45, outBrush);
    canvas.drawCircle(center + Offset(0, radius * -houseOffsetRatio),
        radius * 0.05, outBrush);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
